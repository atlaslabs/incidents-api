## CAD Hub API Examples

### Project Setup

To run the sample code, download and install Visual Studio 2022 (free Community Edition available from https://visualstudio.microsoft.com), then modify the project's `appsettings.json` file to specify the client ID and secret provided by Atlas Labs.

### Authentication

The CAD Hub API utilizes the IdentityServer4 library for OpenID Connect and OAuth 2.0 compatible clients. For further information on the authentication framework and additional samples, see the IdentityServer4 documentation at https://docs.identityserver.io/en/latest/index.html.

