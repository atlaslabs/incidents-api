﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Atlas.CadHub.ConsoleApiClient.Model
{
    public class IncidentDto
    {
        public string AgencyId { get; set; }
        [MaxLength(30)]
        public string TransactionId { get; set; }
        [MaxLength(30)]
        public string IncidentNumber { get; set; }
        [MaxLength(30)]
        public string EventNumber { get; set; }
        [MaxLength(30)]
        public string CaseNumber { get; set; }
        [MaxLength(10)]
        public string CallSource { get; set; }
        [MaxLength(15)]
        public string StreetNumber { get; set; }
        [MaxLength(2)]
        public string Predirectional { get; set; }
        [MaxLength(100)]
        public string StreetName { get; set; }
        [MaxLength(10)]
        public string StreetSuffix { get; set; }
        [MaxLength(2)]
        public string Postdirectional { get; set; }
        [MaxLength(15)]
        public string Suite { get; set; }
        [MaxLength(15)]
        public string Building { get; set; }
        [MaxLength(15)]
        public string Floor { get; set; }
        [MaxLength(60)]
        public string CityOrLocality { get; set; }
        [MaxLength(50)]
        public string County { get; set; }
        [MaxLength(50)]
        public string StateOrProvince { get; set; }
        [MaxLength(15)]
        public string PostalCode { get; set; }
        [MaxLength(100)]
        public string CommonPlaceName { get; set; }
        [MaxLength(50)]
        public string CrossStreet1 { get; set; }
        [MaxLength(50)]
        public string CrossStreet2 { get; set; }
        [Range(-180, 180)]
        public double Longitude { get; set; }
        [Range(-90, 90)]
        public double Latitude { get; set; }
        public bool PublicLocation { get; set; }
        public string LocationComment { get; set; }
        public string Jurisdiction { get; set; }
        [MaxLength(15)]
        public string PulsePointIncidentCallType { get; set; }
        [MaxLength(15)]
        public string AgencyIncidentCallType { get; set; }
        [MaxLength(100)]
        public string AgencyIncidentCallTypeDescription { get; set; }
        [MaxLength(15)]
        public string AgencyIncidentCallSubType { get; set; }
        [MaxLength(100)]
        public string AgencyIncidentCallSubTypeDescription { get; set; }
        [MaxLength(15)]
        public string PulsePointDeterminantCode { get; set; }
        [MaxLength(15)]
        public string AgencyDeterminantCode { get; set; }
        [MaxLength(2)]
        public string AlarmLevel { get; set; }
        [MaxLength(15)]
        public string Priority { get; set; }

        [MaxLength(50)]
        public string CommandName { get; set; }
        [MaxLength(15)]
        public string TacticalChannel { get; set; }
        public DateTime? CallReceivedDateTime { get; set; }
        public DateTime? DispatchDateTime { get; set; }
        public DateTime? EnrouteDateTime { get; set; }
        public DateTime? OnSceneDateTime { get; set; }
        public DateTime? AmbulanceArrivalDateTime { get; set; }
        public DateTime? PatientSideDateTime { get; set; }
        public DateTime? ClosedDateTime { get; set; }
        public DateTime? EntryDateTime { get; set; }
        public DateTime IncidentLastUpdate { get; set; }
        public bool SuppressInApp { get; set; }

        public UnitDto[] Unit { get; set; }

        public IncidentDto()
        {
            // Make compatible with IEnumerable (should always enumerate)
            Unit = new UnitDto[] { };
        }
    }
}