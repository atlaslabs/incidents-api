﻿namespace Atlas.CadHub.ConsoleApiClient.Model
{
    public class ApiResult
    {
        /// <summary>
        /// Same as HTTP code more or less
        /// </summary>
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }

        public static readonly ApiResult Success = new ApiResult { StatusCode = "200", StatusDescription = "Success" };
    }
}
