﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Atlas.CadHub.ConsoleApiClient.Model
{
    public class UnitDto
    {
        [MaxLength(30)]
        public string UnitDispatchNumber { get; set; }
        [MaxLength(20)]
        public string UnitId { get; set; }
        [MaxLength(10)]
        public string VehicleNumber { get; set; }
        [MaxLength(10)]
        public string PulsePointDispatchStatus { get; set; }
        [MaxLength(10)]
        public string AgencyDispatchStatus { get; set; }
        [MaxLength(10)]
        public string ResponsePriority { get; set; }

        public DateTime? UnitDispatchDateTime { get; set; }
        public DateTime? UnitEnrouteDateTime { get; set; }
        public DateTime? UnitOnSceneDateTime { get; set; }
        public DateTime? UnitTransportStartDateTime { get; set; }
        public DateTime? UnitTransportArriveDateTime { get; set; }
        public DateTime? UnitAvailableOnEventDateTime { get; set; }
        public DateTime? UnitClearedDateTime { get; set; }
        [MaxLength(50)]
        public string TransportLocation { get; set; }
        [MaxLength(20)]
        public string TransportPriority { get; set; }
        public double TransportMileageStart { get; set; }
        public double TransportMileageEnd { get; set; }
    }
}
