﻿using Atlas.CadHub.ConsoleApiClient.Model;
using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Atlas.CadHub.ConsoleApiClient
{
    /// <summary>
    /// Example incident submission using IdentityModel library for requesting a token. For additional information, see 
    /// https://identitymodel.readthedocs.io/en/latest/client/token.html#requesting-a-token-using-the-password-grant-type 
    /// </summary>
    internal class ApiTestClient
    {
        private IConfiguration Config { get; }
        private ILogger Logger { get; }

        public ApiTestClient(ILogger<ApiTestClient> logger, IConfiguration config)
        {
            Logger = logger;
            Config = config;
        }

        private async Task<TokenResponse> GetAccessToken()
        {
            // discover endpoints from metadata
            var client = new HttpClient();
            var config = Config.GetSection("Identity");
            var rootUrl = config["RootUrl"]?.TrimEnd('/')
                ?? throw new ApplicationException("RootUrl not specified in appsettings.json file");

            // See https://cad.atlaslabs.io/v1/.well-known/openid-configuration for discovery document
            var disco = await client.GetDiscoveryDocumentAsync(rootUrl);

            if (disco.IsError)
            {
                Logger.LogError("Error reading discovery document: {Error}", disco.Error);
                return null;
            }

            // Request client token from /connect/token endpoint
            var request = new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = config["ClientId"],
                ClientSecret = config["ClientSecret"],
                Scope = config["Scope"] ?? "CadHub"
            };
            var tokenResp = await client.RequestClientCredentialsTokenAsync(request);

            if (tokenResp?.IsError ?? true)
            {
                Logger.LogError("Error getting access token: {Error}", tokenResp?.Error);
            }
            else
            {
                var expires = DateTime.Now.AddSeconds(tokenResp?.ExpiresIn ?? 0);
                Logger.LogInformation("Received access token {Token}\r\n\tExpires at {ExpireDate} ({ExpireSeconds} seconds)", tokenResp?.AccessToken, expires, tokenResp?.ExpiresIn);
            }

            return tokenResp;
        }

        private IncidentDto CreateIncident()
        {
            return new IncidentDto
            {
                AgencyId = "07035",
                TransactionId = "50000191",
                IncidentNumber = "1100091597",
                EventNumber = "F11002077",
                CaseNumber = "12581258",
                CallSource = "PBX",
                StreetNumber = "1500",
                Predirectional = "S",
                StreetName = "Main",
                StreetSuffix = "St",
                Postdirectional = "W",
                Suite = "202",
                Building = "5",
                Floor = "2",
                CityOrLocality = "San Ramon",
                County = "Contra Costa",
                StateOrProvince = "CA",
                PostalCode = "94583",
                CommonPlaceName = "Central Park",
                CrossStreet1 = "Monte Sereno Dr",
                CrossStreet2 = "Stone Valley Dr",
                Longitude = -121.994762420654,
                Latitude = 37.7718002733786,
                PublicLocation = true,
                LocationComment = "Patient in Aisle #12",
                Jurisdiction = "E320",
                SuppressInApp = false,
                PulsePointIncidentCallType = "ME",
                AgencyIncidentCallType = "MED",
                AgencyIncidentCallTypeDescription = "Medical Assist",
                AgencyIncidentCallSubType = "SOB",
                AgencyIncidentCallSubTypeDescription = "Shortness of Breath",
                PulsePointDeterminantCode = "09E02",
                AgencyDeterminantCode = "CPR01",
                Priority = "Echo",
                CommandName = "Sycamore IC",
                TacticalChannel = "TAC22",
                CallReceivedDateTime = DateTime.UtcNow,
                Unit = new UnitDto[]
                {
                    new UnitDto
                    {
                        UnitDispatchNumber = "10009087",
                        UnitId = "E31",
                        VehicleNumber = "336",
                        PulsePointDispatchStatus = "OS",
                        AgencyDispatchStatus = "XC",
                        ResponsePriority = "2"
                    }
                }
            };
        }

        public async Task SubmitIncident(CancellationToken cancellationToken = default)
        {
            var token = await GetAccessToken();

            if (string.IsNullOrEmpty(token?.AccessToken))
            {
                Logger.LogError("Access token null. Cannot proceed");
                return;
            }

            var config = Config.GetSection("Identity");
            var apiClient = new HttpClient();
            apiClient.DefaultRequestHeaders.Accept.Clear();
            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            apiClient.SetBearerToken(token.AccessToken);

            var incident = CreateIncident();
            var url = config["RootUrl"] + config["IncidentsEndpoint"];

            Logger.LogInformation("Performing HTTP POST to {Url}", url);
            var resp = await apiClient.PostAsJsonAsync(url, incident);

            if (!resp.IsSuccessStatusCode)
            {
                Logger.LogError("API call failed: {StatusCode} - {StatusMessage}", resp.StatusCode, resp.ReasonPhrase);
            }
            else
            {
                var content = await resp.Content.ReadFromJsonAsync<ApiResult>();
                Logger.LogDebug("Received from API: \r\n{contents}", content);

                if (content.StatusCode == "200")
                    Logger.LogInformation("Incident submission successful");
            }
        }
    }
}
