﻿using Atlas.CadHub.ConsoleApiClient;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
    services.AddSingleton<ApiTestClient>())
    .Build();

await host.Services.GetRequiredService<ApiTestClient>()
    .SubmitIncident();
// await host.RunAsync();